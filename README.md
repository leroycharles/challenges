# My Useless Challenges
## Introduction
This repo contains differents questions I asked myslef like:

- How does *this* works ? *Why* ?
- Can I do *this* automatically ?
- I know *this solution* exist for *this system/configuration/situation*, can I adapt it to *that other system/configuration/situation* ? Why ?
- There must be a quicker way to do *this*, can I do it ?

## Additionnal Informations
Each questions will be added as an issue that will result in a merge request.

I'll be happy to be help during my research but I do **not** want the solution right away (where is the fun in that ?). If you want to help, just write a comment in the corresponding issue or send me an e-mail at leroy.charles.forum@gmail.com .

This repo is hosted on GitLab.com and uses MkDocs to render a static web-site.

The different solutions are compressed with 7-zip and all documents are generated with Pandoc and LaTeX.

You can contact me via my blog leroycharles.vivaldi.net

## Pending Questions

- HELLO WORLD
- HELLO WORLD

## On-going Questions

- I AM IN HELL
- I AM IN HELL

## Closed Question

- I HAVE SEEN HELL
- I HAVE SEEN HELL
